# Aliases

# Dir
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

# Navigation
alias ..='cd ..'
alias ...='cd ../..'
# Grep
alias grep='grep --color=auto' 
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# List 
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls -h --color=auto'
alias lx='ls -X'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'

# Utilities
alias wget='wget -c'

# Typos
alias celar='clear'
alias cleawr='clear'
